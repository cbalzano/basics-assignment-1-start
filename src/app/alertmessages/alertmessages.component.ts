import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alertmessages',
  template: '<app-warningalert></app-warningalert><app-succesalert></app-succesalert>',
  styleUrls: ['./alertmessages.component.css']
})
export class AlertmessagesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
