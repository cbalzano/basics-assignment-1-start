import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { WarningalertComponent } from './warningalert/warningalert.component';
import { SuccesalertComponent } from './succesalert/succesalert.component';
import { AlertmessagesComponent } from './alertmessages/alertmessages.component';

@NgModule({
  declarations: [
    AppComponent,
    WarningalertComponent,
    SuccesalertComponent,
    AlertmessagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
